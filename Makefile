MODULE_big = json
OBJS = json.o jsonpath.o json_io.o json_op.o util.o compat.o

DATA_built = json.sql
DATA = uninstall_json.sql
REGRESS = init json validate condense orig json_path json_get json_set array_to_json unicode

ifdef USE_PGXS
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
else
subdir = contrib/json
top_builddir = ../..
include $(top_builddir)/src/Makefile.global
include $(top_srcdir)/contrib/contrib-global.mk
endif
