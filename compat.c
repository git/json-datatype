/*-------------------------------------------------------------------------
 *
 * compat.c
 *	  Compatibility routines to let the JSON module work in PostgreSQL 8.3
 *
 * Copyright (c) 2011, PostgreSQL Global Development Group
 * Arranged by Joey Adams <joeyadams3.14159@gmail.com>.
 *
 *-------------------------------------------------------------------------
 */

#include "compat.h"


