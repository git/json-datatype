/*-------------------------------------------------------------------------
 *
 * compat.h
 *	  Compatibility routines to let the JSON module work in PostgreSQL 8.3
 *
 * Copyright (c) 2011, PostgreSQL Global Development Group
 * Arranged by Joey Adams <joeyadams3.14159@gmail.com>.
 *
 *-------------------------------------------------------------------------
 */

#ifndef JSON_COMPAT_H
#define JSON_COMPAT_H

/*** 8.4.3 ***/

#ifndef SearchSysCacheList1
	#define SearchSysCacheList1(cacheId, key1) \
		SearchSysCacheList(cacheId, 1, key1, 0, 0, 0)
#endif

#endif
