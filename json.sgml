<sect1 id="json">
 <title>json</title>

 <indexterm zone="json">
  <primary>json</primary>
 </indexterm>

 <para>
  This module implements the <type>json</> data type for storing <ulink url="http://www.json.org/">JSON</ulink> content in <productname>PostgreSQL</>.  The advantage of using the <type>json</> type over storing JSON content in a <type>text</> field is that it makes sure input values are valid JSON, and there are several type-safe functions for manipulating JSON content.
 </para>

 <para>
  The <type>json</> type stores valid JSON <quote>values</quote> as defined by <ulink url="http://json.org/">json.org</ulink>.  That is, a <type>json</> field can hold a string, number, object, array, 'true', 'false', or 'null'.
 </para>

 <para>
  The <type>json</> datatype should be thought of as a specialization of <type>text</type> rather than a wrapper around <type>text</>, <type>int</>, <type>float</>, etc.  For instance, <literal>' "string" '::json::text</literal> will simply yield <literal>' "string" '</literal>.  Also, bear in mind that JSON null (<literal>'null'::json</literal>) and SQL NULL (<literal>NULL::json</literal>) are two different things.
 </para>

 <para>
  The json module is currently under development.
 </para>

 <sect2>
  <title><type>json</> Functions</title>

  <table id="json-func-table">
   <title><type>json</type> Functions</title>

   <tgroup cols="5">
    <thead>
     <row>
      <entry>Function</entry>
      <entry>Return Type</entry>
      <entry>Description</entry>
      <entry>Example</entry>
      <entry>Result</entry>
     </row>
    </thead>

    <tbody>
     <row>
      <entry morerows="1"><function>to_json(anyelement)</function></entry>
      <entry morerows="1"><type>json</type></entry>
      <entry morerows="1">Encode a value as JSON.</entry>
      <entry><literal>to_json('string'::TEXT)</literal></entry>
      <entry><literal>'"string"'</literal></entry>
     </row>
     <row>
      <entry><literal>to_json(array['one','two','three',null]::text[])</literal></entry>
      <entry><literal>'["one","two","three",null]'</literal></entry>
     </row>
     <row>
      <entry><function>from_json(json)</function></entry>
      <entry><type>text</type></entry>
      <entry>Decode a JSON-encoded value.</entry>
      <entry><literal>from_json('"string"')</literal></entry>
      <entry><literal>'string'</literal></entry>
     </row>
     <row>
      <entry morerows="1"><function>json_validate(text)</function></entry>
      <entry morerows="1"><type>boolean</type></entry>
      <entry morerows="1">Determine if text is valid JSON.</entry>
      <entry><literal>json_validate('{key: "value"}')</literal></entry>
      <entry><literal>false</literal></entry>
     </row>
     <row>
      <entry><literal>json_validate('{"key": "value"}')</literal></entry>
      <entry><literal>true</literal></entry>
     </row>
     <row>
      <entry morerows="2"><function>json_get(json, jsonpath text)</function></entry>
      <entry morerows="2"><type>json</type></entry>
      <entry morerows="2">Select a single value from a JSON tree using a JSONPath expression.</entry>
      <entry><literal>json_get('[0,1,2]', '$[1]')</literal></entry>
      <entry><literal>'1'</literal></entry>
     </row>
     <row>
      <entry><literal>json_get('[0,1,2]', '$[100]')</literal></entry>
      <entry><literal>NULL</literal></entry>
     </row>
     <row>
      <entry><literal>json_get('[0,1,2]', '$[*]')</literal></entry>
      <entry><literal>Error</literal></entry>
     </row>
     <row>
      <entry morerows="2"><function>json_set(json, jsonpath text, json)</function></entry>
      <entry morerows="2"><type>json</type></entry>
      <entry morerows="2">Set items in a JSON tree that match a JSONPath expression.</entry>
      <entry><literal>json_set('[0,1,2]', '$[1]', '"x"')</literal></entry>
      <entry><literal>'[0,"x",2]'</literal></entry>
     </row>
     <row>
      <entry><literal>json_set('[0,1,2]', '$[100]', '"x"')</literal></entry>
      <entry><literal>'[0,1,2]'</literal></entry>
     </row>
     <row>
      <entry><literal>json_set('[0,1,2]', '$[*]', '"x"')</literal></entry>
      <entry><literal>'["x","x","x"]'</literal></entry>
     </row>
     <row>
      <entry morerows="2"><function>json_path(json, jsonpath text)</function></entry>
      <entry morerows="2"><type>setof json</type></entry>
      <entry morerows="2">Select multiple values from a JSON tree using a JSONPath expression.</entry>
      <entry><literal>json_path('[0,1,2]', '$[1]')</literal></entry>
      <entry>
<programlisting>
 1
(1 row)
</programlisting>
      </entry>
     </row>
     <row>
      <entry><literal>json_path('[0,1,2]', '$[100]')</literal></entry>
      <entry>
<programlisting>
(0 rows)
</programlisting>
      </entry>
     </row>
     <row>
      <entry><literal>json_path('[0,1,2]', '$[*]')</literal></entry>
      <entry>
<programlisting>
 0
 1
 2
(3 rows)
</programlisting>
      </entry>
     </row>
     <row>
      <entry morerows="1"><function>json_condense(json)</function></entry>
      <entry morerows="1"><type>json</type></entry>
      <entry morerows="1">Re-encodes JSON to form a string with minimal length (mainly removes whitespace).</entry>
      <entry><literal>json_condense('    {  "key" : "value"}    ')</literal></entry>
      <entry><literal>'{"key":"value"}'</literal></entry>
     </row>
     <row>
      <entry><literal>json_condense($$  "\u266B"  $$)</literal></entry>
      <entry><literal>'"&#9835;"' -- if encoding supports Unicode</literal></entry>
     </row>
     <row>
      <entry><function>json_get_type(json)</function></entry>
      <entry><type>json_type</type>&nbsp;-&nbsp;&nbsp;one&nbsp;of:
<programlisting>
'null'
'string'
'number'
'bool'
'object'
'array'
</programlisting>
      </entry>
      <entry>Get the type of a <type>json</type> value.</entry>
      <entry><literal>json_get_type('{"pi": "3.14159", "e": "2.71828"}')</literal></entry>
      <entry><literal>'object'</literal></entry>
     </row>
    </tbody>
   </tgroup>
  </table>

 </sect2>

 <sect2>
  <title>Author</title>

  <para>
   Joey Adams <email>joeyadams3.14159@gmail.com</email>
  </para>
  
  <para>
   Development of this module was sponsored by Google through its Google Summer of Code program (<ulink url="http://code.google.com/soc">code.google.com/soc</ulink>).
  </para>
 </sect2>

</sect1>
