/*-------------------------------------------------------------------------
 *
 * jsonpath.h
 *	  JSONPath implementation routines for JSON data type support.
 *
 *	  The "JSONPath" implemented here is similar to, but not exactly the same as,
 *	  the JSONPath described at http://goessner.net/articles/JsonPath/ .
 *	  The main differences are stronger subscripting rules, special methods
 *	  via function-call notation (currently, the only one provided is .char()),
 *	  and that the '$' at the beginning of a JSONPath expression is optional.
 *	  Also, array indices as a set (e.g. [0,1]) and filters/scripts are currently
 *	  not implemented.	Array indices are a planned feature.  True filters
 *	  would require an honest-to-goodness JavaScript engine, so perhaps
 *	  someone will write a module for that in the future.
 *
 * Copyright (c) 2010, PostgreSQL Global Development Group
 * Written by Joey Adams <joeyadams3.14159@gmail.com>.
 *
 *-------------------------------------------------------------------------
 */

#ifndef JSONPATH_H
#define JSONPATH_H

#include "json.h"

#include "nodes/pg_list.h"

typedef enum
{
	JP_ROOT,
	JP_WILDCARD,
	JP_INDEX_SUBSCRIPT,
	JP_KEY_SUBSCRIPT,
	JP_CALL_CHAR
}	jp_element_type;

/*
 * A jp_element is a single piece of a JSONPath expression
 * (e.g. [3], .foo, .char(3), ..*).  It represents subscripting
 * down one level in a JSON tree, or invoking a special method
 * (like .char() ).  However, if recursive_descent is set to true,
 * the subscript applies to a value and all of its descendants.
 */
typedef struct
{
	jp_element_type type;

	union
	{
		long		index;
		struct
		{
			char	   *ptr;
			size_t		length;
		}			key;
	}			data;

	/* If element was preceded by ".." in pattern */
	bool		recursive_descent;
}	jp_element;

typedef enum
{
	JP_REF_NODE,
	JP_REF_CHAR
}	JPRefType;

/*
 * A JPRef is a reference to some part of a JSON tree,
 * typically a value (but not always).
 *
 * JPRef* is really a "subclass" of JSON* .  In the future, this structure
 * will likely be merged into the JSON structure to improve performance.
 */
typedef struct
{
	JPRefType	type;

	union
	{
		JSON	   *node;

		struct
		{
			const char *bytes;
			size_t		length;
		} chr;
	}			u;
}	JPRef;

typedef List /* jp_element* */ JSONPath;

JSONPath   *jp_parse(const char *pattern);
char	   *jp_show(JSONPath * jp);

List /* JPRef* */ *jp_match(JSONPath * jp, JSON * json);
void		jp_set(JSONPath * jp, JSON * json, JSON * value);

/* Returns the JSON encoding of the given reference. */
char	   *jpref_encode(JPRef * ref);

#endif
