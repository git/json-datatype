SELECT json_condense('"hello"');
SELECT json_condense($$"hello\u266Bworld"$$);
SELECT json_condense($$"hello\u266bworld"$$);
SELECT json_condense($$"hello♫world"$$);
SELECT json_condense($$      "hello world"    $$);
SELECT json_condense($$    {  "hello" : "world"}    $$);
SELECT json_condense($$    {  "hello" : "world", "bye": 0.0001  }    $$);
SELECT json_condense($$    {  "hello" : "world",
								"bye": 0.0000001
}    $$);
SELECT json_condense($$    {  "hello" : "world"
,
"bye"
: [-0.1234e1, 12345e0]		}    $$);
