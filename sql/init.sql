SET client_min_messages = warning;
\set ECHO none
\i json.sql

\i sql/test_strings.sql
CREATE TABLE valid_test_strings AS
	SELECT string FROM test_strings WHERE json_validate(string);

\set ECHO all
RESET client_min_messages;
