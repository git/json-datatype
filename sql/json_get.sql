SELECT json_get('{"key": "value", "key2": "value2"}', 'key');
SELECT json_get('{"key": "value", "key": "value2"}', 'key');

/* These each return no result because 0 is a number
   (indicating a numeric subscript),
   not a string (indicating an object subscript). */
SELECT json_get('{"0": "value", "key": "value"}', '0');
SELECT json_get('{"0": "value", "0": "value"}', '[0]');
SELECT json_get('{"0": "value", "0": "value"}', '.0');

SELECT json_get('{"0": "value", "1": "value"}', '["0"]');
SELECT json_get('{"0": "value", "0": "value"}', '["0"]');

SELECT json_get('[0,1,2,3]', '0');
SELECT json_get('[0,1,2,3]', '"0"');
SELECT json_get('[0,1,2,3]', '*');
SELECT json_get('[0]', '*');
SELECT json_get('[[0]]', '..*');
