SELECT parse_json_path('..');
SELECT parse_json_path('..*');
SELECT parse_json_path('.*');
SELECT parse_json_path('');
SELECT parse_json_path('$.*');
SELECT parse_json_path('$');
SELECT parse_json_path('$*');
SELECT parse_json_path('*');
SELECT parse_json_path($$ .. [ 025  ] $$);
SELECT parse_json_path($$ $ . 0 . 3 $$);
SELECT parse_json_path($$ $ . 0_3 $$);
SELECT parse_json_path($$ $ . 0.3 $$);
SELECT parse_json_path($$ .. [ 031  ] $$);
SELECT parse_json_path($$ $ . 0 . 3 . hello $$);
SELECT parse_json_path($$ $ . 0 . 3 . hello . "back\\slash" $$);
SELECT parse_json_path($$ $ . 0 . 3 . hello . ["back\\slash"] $$);
SELECT parse_json_path($$ $ . 0 . 3 . hello . back\slash $$);
SELECT parse_json_path($$ $ . 0 . 3 . hello . backslash $$);
SELECT parse_json_path($$ .. [ 0x31  ] $$);
SELECT parse_json_path($$ [ 0x31  ] $$);
SELECT parse_json_path($$ _3 $$);
SELECT parse_json_path($$ _3_ $$);
SELECT parse_json_path($$ [ _3 ] $$);
SELECT parse_json_path($$ $ _3 $$);
SELECT parse_json_path($$ $ . _3 $$);
SELECT parse_json_path('..["5"]');
SELECT parse_json_path('..[5]');
SELECT parse_json_path('..5');
SELECT parse_json_path($$ .. [ -5  ] $$);
SELECT parse_json_path($$ .. [ "5"  ] $$);
SELECT parse_json_path($$ .. [ +5  ] $$);
SELECT parse_json_path($$ .. [ 5  ] $$);
SELECT parse_json_path($$ .. ["5"] $$);
SELECT parse_json_path($$ ..["5"] $$);
SELECT parse_json_path($$ [ "5" ] $$);
SELECT parse_json_path($$ [ 5 ] $$);
SELECT parse_json_path('."hello"');
SELECT parse_json_path('.hello');
SELECT parse_json_path('...["hello world"]');
SELECT parse_json_path('..["hello world"]');
SELECT parse_json_path('."hello world"');
SELECT parse_json_path('.["hello world"]');
SELECT parse_json_path('.hello world');
SELECT parse_json_path('..["hello world"]["5"]');
SELECT parse_json_path('..["hello world"][5]');
SELECT parse_json_path('..["hello world"][5]..[3]..*.[*]');
SELECT parse_json_path('..["hello world"][5]..[3]..*');
SELECT parse_json_path('..["hello world"][5]..[3]');
SELECT parse_json_path('..["hello world"][5]..[3]*');
SELECT parse_json_path($$
	.. [ '"hello"\\"world"' ]
	[ 5 ] .. [3] . * [ * ]
$$);

select parse_json_path('$char');
select parse_json_path('$.char');
select parse_json_path('$.char()');
select parse_json_path('$.char(5)');
select parse_json_path('$.char(  -1  )');
select parse_json_path('$.char(  -1  )  . char(0)');
select parse_json_path('$.char(  -1  )  . char  ( 0 )  ');
select parse_json_path('$.char(  +1  )  . char  ( 0 )  ');
select parse_json_path('$ . char(  +1  )  . char  ( 0 )  ');
select parse_json_path('$ .. char(  +1  )  . char  ( 0 )  ');
select parse_json_path('$ .. char(  +1  )  .. char  ( 0 )  ');
select parse_json_path('$ . char(  +1  )  .. char  ( 0 )  ');
select parse_json_path('$ . char(  +1  )  char  ( 0 )  ');
select parse_json_path('$ char(  +1  )  char  ( 0 )  ');
select parse_json_path('char(  +1  )  . char  ( 0 )  ');
select parse_json_path('.char(  +1  )  . char  ( 0 )  ');
select parse_json_path('..char(  +1  )  . char  ( 0 )  ');
select parse_json_path('...char(  +1  )  . char  ( 0 )  ');
select parse_json_path('[char(5)]');

SELECT json_path($$"⁰¹\u0000³"$$, 'char(0)');
SELECT json_path($$"⁰¹\u0000³"$$, 'char(1)');
SELECT json_path($$"⁰¹\u0000³"$$, 'char(2)');
SELECT json_path($$"⁰¹\u0000³"$$, 'char(3)');
SELECT json_path('"0123"', 'char(-1)');
SELECT json_path('"0123"', 'char(0)');
SELECT json_path('"0123"', 'char(1)');
SELECT json_path('"0123"', 'char(2)');
SELECT json_path('"0123"', 'char(3)');
SELECT json_path('"0123"', 'char(4)');
SELECT json_path('"⁰¹²³"', 'char(0)');
SELECT json_path('"⁰¹²³"', 'char(1)');
SELECT json_path('"⁰¹²³"', 'char(2)');
SELECT json_path('"⁰¹²³"', 'char(3)');
SELECT json_path('[1,2,3]', '$');
SELECT json_path('[1,2,3]', '$.+1');
SELECT json_path('[1,2,3]', '$.-1');
SELECT json_path('[1,2,3]', '$.0');
SELECT json_path('[1,2,3]', '$.1');
SELECT json_path('[1,2,3]', '$.2');
SELECT json_path('[1,2,3]', '$.3');
SELECT json_path('[1,2,3]', '*');
SELECT json_path('[1,2,3]', '[0]');
SELECT json_path('[1,2,3]', '[1]');
SELECT json_path('[1,2,3]', '[2]');
SELECT json_path('{"0": "zero", "1": "one", "2": "two"}', $$['2']$$);
SELECT json_path('{"0": "zero", "1": "one", "2": "two"}', '["0"]');
SELECT json_path('{"0": "zero", "1": "one", "2": "two"}', '["1"]');
SELECT json_path('{"0": "zero", "1": "one", "2": "two"}', '["2"]');
SELECT json_path('{"0": "zero", "1": "one", "2": "two"}', '[0]');
SELECT json_path('{"0": "zero", "1": "one", "2": "two"}', '[1]');

-- Multiple JSONPath results are listed in BFS order
-- (just because it's easier to implement efficiently than DFS)
SELECT json_path('[0,1,[2, [3,4 , 5],6,[7,8],9],10]', '$..*');


CREATE TABLE sample (json JSON);
INSERT INTO sample VALUES ($$
{
	"store": {
		"books": [
			{
				"title": "book 0",
				"author": "author 0",
				"prices": [1,2,3]
			}, {
				"title": "book 1",
				"author": "author 1",
				"prices": [4,5,6]
			}, {
				"title": "book 2",
				"author": "author 2",
				"prices": [7,8,9]
			}
		],
		"toys": [
			"Yo-yo",
			"Boomerang",
			"Basketball"
		]
	}
}
$$);

-- Use unaligned output so results are consistent between PostgreSQL 8 and 9.
\a

SELECT json_path(json, 'store.books[*]') FROM sample;
SELECT json_path(json, 'store.books[*].title') FROM sample;
SELECT json_path(json, 'store.books[*].author') FROM sample;
SELECT json_path(json, 'store.books[*].prices') FROM sample;
SELECT json_path(json, 'store.books[*].prices[*]') FROM sample;
SELECT json_path(json, 'store.toys[*]') FROM sample;
SELECT json_path(json, 'store.toys[*][0]') FROM sample;
SELECT json_path(json, 'store.toys[*][1]') FROM sample;
SELECT json_path(json, 'store.toys[*][0][0]') FROM sample;
SELECT json_path(json, 'store.toys[*][0][1]') FROM sample;

SELECT json_path(json, '..books') FROM sample;
SELECT json_path(json, '..books[*]') FROM sample;
SELECT json_path(json, '..title') FROM sample;
SELECT json_path(json, '..author') FROM sample;
SELECT json_path(json, '..prices[*]') FROM sample;
SELECT json_path(json, '..toys[*]') FROM sample;
SELECT json_path(json, '..toys..[*]') FROM sample;

SELECT json_path(json, '..[-1]') FROM sample;
SELECT json_path(json, '..[0]') FROM sample;
SELECT json_path(json, '..[1]') FROM sample;
SELECT json_path(json, '..[2]') FROM sample;
SELECT json_path(json, '..[3]') FROM sample;

SELECT json_path(json, '$') FROM sample;
SELECT json_path(json, '..*') FROM sample;

SELECT json_path(json, '..char(-1)') FROM sample;
SELECT json_path(json, '..char(0)') FROM sample;
SELECT json_path(json, '..char(1)') FROM sample;
SELECT json_path(json, '..char(2)') FROM sample;
SELECT json_path(json, '..char(3)') FROM sample;
SELECT json_path(json, '..char(100)') FROM sample;

-- Turn aligned output back on.
\a
