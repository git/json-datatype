SELECT json_set('[1,2,3]', '[1]', 5::json);
SELECT json_set('[1,2,3]', '[3]', 5::json);
SELECT json_set('[1,2,3]', '[2]', 5::json);
SELECT json_set('[1,2,3]', '[2]', '[0,1]'::json);
SELECT json_set('  [ 1 ,   2,3]', '1', '5');
SELECT json_set('  [ 1 ,   [2,3],3]', '1', '5');
SELECT json_set('  [ 1 ,   [  2 ,   3     ],3]', '[1][0]', '5');
SELECT json_set('  [ 1 ,   [  2 ,   3     ],3]', '[5][0]', '5');
SELECT json_set('  [ 1 ,   [  2 ,   3     ],3]', '[4][0]', '5');
SELECT json_set('  [ 1 ,   [  2 ,   3     ],3]', '[3][0]', '5');
SELECT json_set('  [ 1 ,   [  2 ,   3     ],3]', '[2][0]', '5');
SELECT json_set('  [ 1 ,   [  2 ,   3     ],3]', '[1][0]', '   5  ');
SELECT json_set('  [ 1 ,   [  2 ,   3     ]  , 3  ] ', '[1][0]', '   5  ');
SELECT json_set('  [ 1 ,   [  2 ,   3     ]  , 3  ] ', '[1 ][0]', '   5  ');
SELECT json_set('  [ 1 ,   [  2 ,   3     ]  , 3  ] ', '[1 ][0]', '     5  ');
SELECT json_set('  [ 1 ,   [  2 ,   3     ]  , 3  ] ', '[1 ][0]', $$     "hello\tworld"  $$);
SELECT json_set('  [ 1 ,   [  2 ,   3     ]  , 3  ] ', '[1 ][0]', $$     "hello\u0009world"  $$);
SELECT json_set('  [ 1 ,   [  2 ,   3     ]  , 3  ] ', '[1 ][*]', $$     "hello\u0009world"  $$);
SELECT json_set('  [ 1 ,   [  2 ,   3     ]  , 3  ] ', '$', $$     "hello\u0009world"  $$);
SELECT json_set('       [ 1 ,   [  2 ,   3     ]  , 3  ] ', '$', $$     "hello\u0009world"  $$);

-- Since JavaScript doesn't do anything when you assign to a character subscript,
-- neither will json_set.
SELECT json_set('"hello"', '[0]', '"H"');
SELECT json_set('"hello"', '[0][0]', '"H"');
SELECT json_set('["hello"]', '[0][0]', '"H"');
SELECT json_set('["hello"]', '[0][0][0]', '"H"');

SELECT json_set('[0,1,2,[3,4,5],4]', '$[*]', '["set"]');
SELECT json_set('[0,1,2,[3,4,5],4]', '$[*][*]', '["set"]');
SELECT json_set('[0,1,2,[3,4,5],4]', '$..[*]', '["set"]');
