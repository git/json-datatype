
/* Make sure creating an array literal and subscripting
 * preserves the original string exactly
 * (except for trimming surrounding whitespace). */
SELECT bool_and(btrim(before, E'\t\n\r ') = after) FROM (
	SELECT
		string AS before,
		json_path(('  [ ' || string || '  ] ')::json, '$.[0]')::text AS after
	FROM valid_test_strings
) AS subquery;
