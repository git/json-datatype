-- Make sure the surrogate pair calculation adds 0x10000 rather than ORing it.
SELECT ascii(from_json($$ "\uD840\uDC00" $$));
