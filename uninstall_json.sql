-- Adjust this setting to control where the objects get dropped.
SET search_path = public;

DROP TYPE json CASCADE;
DROP TYPE json_type;

DROP FUNCTION json_validate(text);
DROP FUNCTION parse_json_path(text);
