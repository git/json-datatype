/*-------------------------------------------------------------------------
 *
 * util.h
 *	  General purpose routines used by JSON data type support.
 *
 * Copyright (c) 2010, PostgreSQL Global Development Group
 * Written by Joey Adams <joeyadams3.14159@gmail.com>.
 *
 *-------------------------------------------------------------------------
 */

#ifndef JSON_UTIL_H
#define JSON_UTIL_H

#include "postgres.h"

#include "funcapi.h"
#include "mb/pg_wchar.h"
#include "utils/lsyscache.h"

typedef struct
{
	Oid			type;
	IOFuncSelector which_func;
	MemoryContext mcxt;

	int16		typlen;
	bool		typbyval;
	char		typalign;
	char		typdelim;
	Oid			typioparam;
	Oid			typiofunc;
	FmgrInfo	proc;

	char		typcategory;
	bool		typispreferred;
} TypeInfo;

typedef struct
{
	int			index;
	const char *label;
}	EnumLabel;


void		getTypeInfo(TypeInfo *d, Oid type, IOFuncSelector which_func, MemoryContext mcxt);

void		getEnumLabelOids(const char *typname, EnumLabel labels[], Oid oid_out[], int count);

size_t utf8_substring(const char *src, size_t srcbytes,
			   size_t start, size_t length,
			   const char **out_start, size_t *out_bytes);
pg_wchar	utf8_decode_char(const char **sp);
bool		utf8_validate(const char *str, size_t length);

/*
 * Adaptations of pg_do_encoding_conversion for simplifying UTF-8 conversions.
 *
 * These are used frequently in the JSON code because JSON nodes are encoded
 * in UTF-8.  The reason they are encoded in UTF-8 is because we need to
 * be able to handle Unicode escapes, but there is
 * no simple and efficient way to do that with the server encoding.
 *
 * Just like pg_do_encoding_conversion, if no conversion is done, the original
 * pointer given is returned.
 */
char	   *server_to_utf8(const char *str, int len);
char	   *utf8_to_server(const char *str, int len);

/*
 * Variants of text_to_cstring and cstring_to_text for simplifying UTF-8 conversions.
 *
 * Just like text_to_cstring, text_to_utf8_cstring will always return a palloc'd,
 * null-terminated C-string.
 */
char	   *text_to_utf8_cstring(const text *t);
text	   *utf8_cstring_to_text(const char *s);
text	   *utf8_cstring_to_text_with_len(const char *s, int len);


#endif
